$(document).ready(function(){

    var $animation_elements = $('.animation-element');
    var $window = $(window);

    function check_if_in_view() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var window_bottom_position = (window_top_position + window_height);
        
        $.each($animation_elements, function() {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);
            
            //check to see if this current container is within viewport
            if ((element_bottom_position >= window_top_position) &&
                (element_top_position <= window_bottom_position)) {
                $element.addClass('in-view');
            // } else {
            //     $element.removeClass('in-view');
            // }
            }
        });
    }

    //hide menu on scroll
    var didScroll;
    var lastScrollTop = 0;
    var delta = 5;
    var navbarHeight = $('.nav').outerHeight();

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            check_if_in_view();
            didScroll = false;
        }
    }, 250);

    function hasScrolled() {
        var st = $(this).scrollTop();
        // Make sure they scroll more than delta
        if (Math.abs(lastScrollTop - st) <= delta)
            return;

        // If they scrolled down and are past the navbar, add class .nav-up.
        if (st > lastScrollTop) {
            // Scroll Down
            if (st > navbarHeight) {
                $('.nav').addClass('nav-up');
            }
        } else {
            // Scroll Up
            if (st + $(window).height() < $(document).height()) {
                $('.nav').removeClass('nav-up');
            }
        }
        
        lastScrollTop = st;
    }

    
    //mobile menu 

    $('.logo-box').click(function(e) {
        if($(e.target).is('.icon-logo')){
            e.preventDefault();
            return;
        }
        $(this).parent().toggleClass('open');
    });

    $('.close-menu').click(function() {
        $(this).parent().parent().removeClass('open');
    });

    $window.trigger('scroll');
    $('.content-inner-container').on('scroll', function(event){ didScroll = true; });
    $window.on('scroll resize', function(event){ didScroll = true; });

});
